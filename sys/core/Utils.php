<?php
namespace Utilities;
class Utils {

    static $metaData;

    static function setup() {
        self::$metaData = file_get_contents('meta.json', true);
    }

    /**
     * Returns the status of Debug mode.
     *
     * @return mixed
     */
    static function getDebugStatus() {
        $debug = json_decode(self::$metaData, true);
        return $debug['debug'];
    }

    /**
     * Returns the Version of the Utilities functions.
     *
     * @return mixed
     */
    static function version() {
        $version = json_decode(self::$metaData, true);
        return $version['version'];
    }

    /**
     * Prints a string to the CLI.
     *
     * @param string $message
     */
    static function log($message = '') {
        echo $message . PHP_EOL;
    }

}