<?php
class Download implements \Template\ScriptTemplate {

    public function main($arguments) {

        if (empty($arguments)) {
            \Utilities\Utils::log('No arguments passed! please pass help or info to get more details about this issue.');
            exit;
        }

        if (DEBUG) {
            foreach ($arguments as $argument) {
                \Utilities\Utils::log($argument);
            }
        }
        \Utilities\Utils::log(exec('wget -O ' . $arguments[1] . ' ' . $arguments[0]));

    }

    /**
     * Description of what the main() function does.
     * Also describes what arguments can be used with said function.
     */
    function info() {

        $info = [
            'Description' => [
                '',
                'This is the simple downloader for our Script engine.',
                'Download [Address] [Filename]',
                ''
            ],
            'Arguments' => [
                '   info - Get information on this function.',
                ''
            ]
        ];

        foreach ($info as $type=>$strings) {
            foreach ($strings as $string) {
                \Utilities\Utils::log($string);
            }
        }

    }
}